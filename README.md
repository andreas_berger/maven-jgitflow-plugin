# Maven JGit-Flow Plugin

**Current Version: 1.0-alpha21.1**

The Maven JGit-Flow Plugin is based on and a replacement for the maven-release-plugin to enable git-flow release workflows.

This plugin also provides support for other git-flow tasks like managing features and hotfixes.

For more information and usage guide, [see the wiki](https://bitbucket.org/atlassian/maven-jgitflow-plugin/wiki)

For discussion about the plugin, [see the google group](https://groups.google.com/forum/#!forum/maven-jgitflow-users)

To log an issue or feature request, [use the issue tracker](https://bitbucket.org/atlassian/maven-jgitflow-plugin/issues)

Got something to say?  [@sysbliss](https://twitter.com/sysbliss) #MavenJGitFlow